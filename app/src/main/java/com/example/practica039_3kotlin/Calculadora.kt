package com.example.practica039_3kotlin

class Calculadora {
    var num1:Int = 0
    var num2:Int = 0
    constructor(num1:Int, num2:Int){
        this.num1 = num1;
        this.num2 = num2;
    }
    fun sumar():Int{
        return num1 + num2;
    }
    fun restar():Int{
        return num1 - num2;
    }
    fun multiplicar():Int{
        return num1 * num2;
    }
    fun dividir():Int{
        var total = 0
        if (num2!=0){
            total = num1/num2;
        }
        return total;
    }
}