package com.example.practica039_3kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class CalculadoraActivity : AppCompatActivity() {
    private lateinit var btnSumar: Button;
    private lateinit var btnRestar: Button;
    private lateinit var btnMultiplicar: Button;
    private lateinit var btnDividir: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnRegresar: Button;

    private lateinit var txtNum1: EditText;
    private lateinit var txtNum2: EditText;
    private lateinit var lblResultado: TextView;
    private var calculadora = Calculadora(0,0);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()

        btnSumar.setOnClickListener {btnSumar()}
        btnRestar.setOnClickListener {btnRestar()}
        btnMultiplicar.setOnClickListener {btnMultiplicar()}
        btnDividir.setOnClickListener {btnDividir()}
        btnRegresar.setOnClickListener {regresar()}
        btnLimpiar.setOnClickListener {limpiar()}

    }

    fun limpiar() {
        txtNum1.setText("");
        txtNum2.setText("");
        lblResultado.setText("");
    }

    fun regresar() {
        var confirmar = AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Deseas regresar?");
        confirmar.setPositiveButton("Confirmar") {
            dialogoInterface, which->finish()
        }
        confirmar.setNegativeButton("Cancelar") {
            dialogoInterface, which->
        }.show()
    }

    private fun iniciarComponentes (){
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)

        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        lblResultado = findViewById(R.id.lblResultado)
    }

    private fun btnSumar(){
        if(validar() != -1) {
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()
            lblResultado.text = calculadora.sumar().toString()
        }
    }

    private fun btnRestar(){
        if(validar() != -1) {
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()
            lblResultado.text = calculadora.restar().toString()
        }
    }

    private fun btnMultiplicar(){
        if(validar() != -1) {
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()
            lblResultado.text = calculadora.multiplicar().toString()
        }
    }

    private fun btnDividir(){
        if(validar() != -1) {
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()
            lblResultado.text = calculadora.dividir().toString()
        }
    }

    private fun validar(): Int {
        if(txtNum1.getText().toString().equals("") || txtNum2.getText().toString().equals("")) {
            Toast.makeText(this.applicationContext, "Ingresar datos", Toast.LENGTH_LONG).show();
            return -1;
        }

        return 0;
    }
}