package com.example.practica039_3kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnIngresar : Button
    private lateinit var btnCerrar : Button
    private lateinit var txtUsuario : EditText
    private lateinit var txtContrasena : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes();
        btnIngresar.setOnClickListener { ingresar() }
        btnCerrar.setOnClickListener {finish()}
    }

    private fun iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar);
        btnCerrar = findViewById(R.id.btnCerrar);
        txtContrasena = findViewById(R.id.txtContrasena);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private fun ingresar() {

        // Asignar los Strings que se declararon en R.Value
        var strUsuario : String = application.resources.getString(R.string.usuario)
        var strContrasena : String = application.resources.getString(R.string.contrasena)

        if(txtUsuario.text.toString() == strUsuario && txtContrasena.text.toString() == strContrasena) {

            // Hacer el paquete de datos que se van a enviar
            var bundle = Bundle();
            bundle.putString("usuario", strUsuario)

            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)

        } else {
            Toast.makeText(this.applicationContext,
                "Usuario o Contraseña no válidos ",
                Toast.LENGTH_LONG).show();
        }
    }
}